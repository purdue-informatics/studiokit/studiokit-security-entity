﻿# StudioKit.Security.Entity

**Note**: Requires the following peer dependencies in the Solution.
* **StudioKit.Data.Entity**
* **StudioKit.Security**

## Interfaces

* IClientDbContext
	* Interface for a DbContext that includes Clients, AppClients, ServiceClients, and RefreshTokens.
